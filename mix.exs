defmodule Quoin.MixProject do
  use Mix.Project

  def project do
    [
      app: :quoin,
      version: "0.0.1",
      elixir: "~> 1.11",
      elixirc_options: [warnings_as_errors: true],
      start_permanent: Mix.env() == :prod,
      name: "Quoin",
      source_url: "https://gitlab.com/JamesFysh/quoin",
      deps: deps(),
      description: description(),
      escript: escript(),
      package: package(),
      aliases: aliases(),
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.23.0"},
      {:floki, "~> 0.30.0"},
      {:pdf, "~> 0.5.0"},
      {:benchee, "~> 1.0"},
    ]
  end

  defp description do
    "Quoin is a library and command-line tool for converting HTML documents to PDFs"
  end

  defp escript do
    [main_module: Quoin.CLI]
  end

  defp package do
    [
      files: ~w(lib .formatter.exs mix.exs README* LICENSE*),
      licenses: ["BSD-3-Clause"],
      links: %{"GitLab": "https://gitlab.com/JamesFysh/quoin"}

    ]
  end

  defp aliases do
    [
      setup: ["deps.get", "escript.build"]
    ]
  end

end
