# Quoin

## Overview

Quoin is a library and command-line tool for converting HTML documents to PDFs.

The intention is to build an Elixir equivalent of the Python [WeasyPrint](https://github.com/Kozea/WeasyPrint) library.
As with WeasyPrint, the library should be able to render HTML and apply CSS but not handle Javascript.  The
downside of this decision is that it will never be possible to use this library to eg. render React views to PDF.

## Roadmap

I know what I want to achieve, I have no idea how to get there.  See [ROADMAP.md](./ROADMAP.md) for more

## Alternatives

As I attempt to work out how to put this library together I have been spending time reading up on other, existing
software for rendering HTML into PDFs, captured in the following list

* [WeasyPrint](https://github.com/Kozea/WeasyPrint)
* [Athena](https://github.com/arachnys/athenapdf)
* [Puppeteer](https://github.com/puppeteer/puppeteer)

## References

Some resources I've read through along the way

[Detailed guide on rendering HTML/CSS](https://www.html5rocks.com/en/tutorials/internals/howbrowserswork)
[CSS2.1 Processing Model](https://www.w3.org/TR/CSS21/intro.html#processing-model)
[CSS2.1 Mediatypes](https://www.w3.org/TR/CSS21/media.html)
[CSS2.1 paged media](https://www.w3.org/TR/CSS21/page.html)
