defmodule Quoin do
  @moduledoc """
  Documentation for `Quoin`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Quoin.hello()
      :world

  """
  def hello do
    :world
  end
end
